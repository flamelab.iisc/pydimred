import os

def dataset_info():
    features='H,H2,O,O2,OH,H2O,HO2,H2O2,CO,CO2,CH2O,HO2CHO,O2CHO,'\
            'CH3O2H,CH3O2,CH4,CH3,C2H5,C2H4,C2H3,CH3CHO,C2H5OH,'\
            'O2C2H4OH,N,NO,NO2,N2O,N2,T,P,vx,vy,vz'
    nx = 672
    ny = 672
    nz = 1
    datafile_prefix = "hcci."
    datafile_suffix = ".field.mpi"
    spatial_dimension = 2
    return (features, nx, ny, nz, os.path.abspath(__file__),
            datafile_prefix, datafile_suffix, spatial_dimension)

