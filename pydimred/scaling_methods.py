'''This module contains the definitions pertaining to scaling of
the datasets.

Currently available methods are:
    1.)  MeanByAbsMax (ref https://doi.org/10.1016/j.jcp.2019.03.003)
'''
import numpy


class Scaler:
    def mean_by_max_abs_scaling(self, data):
        # copy the data so that the original data is unchanged
        X = data.copy()
        # obtain dimensions of data array
        nx, ny, nz, nv = data.shape
        # obtain max_abs and mean
        max_abs, mean = self.evaluate_abs_max_and_mean(data)
        # perform scaling: remove mean by max_abs
        for feature in range(nv):
            X[..., feature] -= mean[feature]
            X[..., feature] /= max_abs[feature]
        return X

    def mean_by_max_abs_unscaling(self, reconstructed_data,
                                  original_data):
        # copy the data so that the reconstructed data is unchanged
        X = reconstructed_data.copy()
        # obtain dimensions of scaled_data array
        nx, ny, nz, nv = reconstructed_data.shape
        # obtain max_abs and mean
        max_abs, mean = self.evaluate_abs_max_and_mean(original_data)
        # rescale the data
        for feature in range(nv):
            X[..., feature] *= max_abs[feature]
            X[..., feature] += mean[feature]
        return X

    def evaluate_abs_max_and_mean(self, data):
        # evaluate maximum of the absolute values for each variable
        absolute = numpy.absolute(data)
        max_abs = absolute.max(axis=(0, 1, 2)) + 1e-14
        # evaluate mean for each variable
        mean = data.mean(axis=(0,1,2))
        return max_abs, mean
