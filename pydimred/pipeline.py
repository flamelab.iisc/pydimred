import numpy, os
from pydimred.datasets import ReadRawData
from pydimred import GetTensors, Scaler, DataReconstruction

class DimRedAnalysis(ReadRawData, Scaler, DataReconstruction):
    pvecs_cov = dict()
    pvecs_kur = dict()
    pvals_cov = dict()
    pvals_kur = dict()

    def __init__(self, dataset):
        if dataset == 'synthetic':
            pass
        else:
            self.set_dataset_details(dataset)
        self.moments = GetTensors()

    def read_saved_data(self, data_path, prefix,
                                  region_label):
        saved_data = numpy.load('{}/{}_{}.npz'.format(
                       data_path, prefix, region_label),
                       allow_pickle=True)

        self.pvecs_cov[region_label] = saved_data['pvecs_cov'][()]
        self.pvecs_kur[region_label] = saved_data['pvecs_kur'][()]
        self.pvals_cov[region_label] = saved_data['pvals_cov'][()]
        self.pvals_kur[region_label] = saved_data['pvals_kur'][()]

    def get_pcs_pvs_for_all_checkpoints(self, region_label,
            xStart=None, xEnd=None, yStart=None, yEnd=None,
            zStart=None, zEnd=None):
        '''Evaluate the principal components and vectors obtained
        using the Co-variance and Co-kurtosis vectors.

        The data is available as class attributes and can be
        accessed by specifying the region_label as keys to the
        following dictionary attributes of the DimRedAnalysis class:
            pvecs_cov[region_label]: principal vectors from CoV
            pvecs_kur[region_label]: principal vectors from CoK
            pvals_cov[region_label]: principal values from CoV
            pvals_kur[region_label]: principal values from CoK

        The data is also saved as an .npz arrays with the name:
        "pcpv_<region_label>.npz" in the CWD.

        Inputs:
        -------
            1.) region_label (str): Label for the region of interest
            2.) xStart (int): starting index of the 0th dimension
                              for the region of interest
            3.) xEnd   (int): ending   index of the 0th dimension
                              for the region of interest
            4.) yStart (int): starting index of the 1st dimension
                              for the region of interest
            5.) yEnd   (int): ending   index of the 1st dimension
                              for the region of interest
            6.) zStart (int): starting index of the 2nd dimension
                              for the region of interest
            7.) zEnd   (int): ending   index of the 2nd dimension
                              for the region of interest

            The default behaviour if no values are provided to the
            start and end points is to consider the entire domain

        Returns: None
        --------
        '''
        # obtain data subdomain extents
        xs, ys, zs, xe, ye, ze = \
            self.obtain_subdomain_extents(xStart, yStart, zStart,
                                        xEnd, yEnd, zEnd)

        # create dicts to store the pcs and pvs for every checkpoint
        pvecs_cov_chkpt = dict()
        pvecs_kur_chkpt = dict()
        pvals_cov_chkpt = dict()
        pvals_kur_chkpt = dict()
        # evaluate pcs and pvs for all checkpoints
        for checkpoint in self.all_checkpoints:
            # read the data
            data = self.read_and_clean_data(checkpoint=checkpoint)
            # isolate subdomain
            data_subdomain = data[xs:xe, ys:ye, zs:ze, :]
            # add evaluate pcs and pvs for current checkpoint
            uCV, uCK, sCV, sCK = self.get_pcs_pvs(data_subdomain)
            # append pcs to dicts
            pvecs_cov_chkpt[checkpoint] = uCV
            pvecs_kur_chkpt[checkpoint] = uCK
            # append pcs to dicts
            pvals_cov_chkpt[checkpoint] = sCV
            pvals_kur_chkpt[checkpoint] = sCK

        # append pcs and pvs to global dicts
        self.pvecs_cov[region_label] = pvecs_cov_chkpt
        self.pvecs_kur[region_label] = pvecs_kur_chkpt
        self.pvals_cov[region_label] = pvals_cov_chkpt
        self.pvals_kur[region_label] = pvals_kur_chkpt

        # save the pcs and pvs for region to disk
        numpy.savez("pcpv_{}".format(region_label),
                pvecs_cov=pvecs_cov_chkpt,
                pvecs_kur=pvecs_kur_chkpt,
                pvals_cov=pvals_cov_chkpt,
                pvals_kur=pvals_kur_chkpt)

    def obtain_subdomain_extents(self, xStart, yStart, zStart,
                               xEnd, yEnd, zEnd):
        '''Return the extents of the simulation domains
        '''
        xs = 0 if isinstance(xStart,type(None)) else xStart
        ys = 0 if isinstance(yStart,type(None)) else yStart
        zs = 0 if isinstance(zStart,type(None)) else zStart
        xe = self.nx if isinstance(xEnd,type(None)) else xEnd
        ye = self.ny if isinstance(yEnd,type(None)) else yEnd
        ze = self.nz if isinstance(zEnd,type(None)) else zEnd
        return (xs, ys, zs, xe, ye, ze)

    def get_pcs_pvs(self, data):
        '''Evaluate the principal components and vectors for a given
        data array

        Inputs: data (ndarray): data array of shape (nx, ny, nz, nv)
        -------

        Returns:
        --------
            1.) uCV (ndarray): array of principal vectors evaluated
                               using the co-variance tensor
                               [shape (nv, nv)]
            2.) uCK (ndarray): array of principal vectors evaluated
                               using the co-kurtosis tensor
                               [shape (nv, nv)]
            3.) sCV (ndarray): array of principal values evaluated
                               using the co-variance tensor
                               [shape (nv,)]
            4.) sCK (ndarray): array of principal values evaluated
                               using the co-variance tensor
                               [shape (nv,)]
        '''
        scaled_data = self.mean_by_max_abs_scaling(data=data)

        cov_tensor, cok_tensor = self.moments.evaluate_tensors(scaled_data)

        uCV,sCV,vCV=numpy.linalg.svd(cov_tensor,full_matrices=False)
        uCK,sCK,vCK=numpy.linalg.svd(cok_tensor,full_matrices=False)

        return uCV, uCK, sCV, sCK

    def get_reconstructed_data_at_checkpoint(self, region,
            checkpoint=None, external_file=None,
            xStart=None, xEnd=None, yStart=None, yEnd=None,
            zStart=None, zEnd=None, retain_features=-1,
            non_participating_species='N2'):
        '''
        '''
        # obtain data subdomain extents
        xs, ys, zs, xe, ye, ze = \
            self.obtain_subdomain_extents(xStart, yStart, zStart,
                                            xEnd, yEnd, zEnd)
        # evalute number of variables to retain for reconstruction
        keep_features = self.n_features if retain_features == -1 \
                    else retain_features
        # read data at checkpoint/from external file
        data = self.read_and_clean_data(checkpoint=checkpoint,
                                        external_file=external_file)
        # isolate subdomain
        data_subdomain = data[xs:xe, ys:ye, zs:ze, :]
        # obtain the scaled data
        scaled_data = self.mean_by_max_abs_scaling(data=data_subdomain)
        # obtain the reduced set of principal vectors from global array
        ## TODO: implement a mechanism to read this from saved data
        ##       somewhere before this
        if isinstance(checkpoint, type(None)):
            # use the PCS and PVS evaluated for some other dataset
            # for generating the reduced manifold
            vCV = self.global_pvecs_cov[:,:keep_features]
            vCK = self.global_pvecs_kur[:,:keep_features]
        else:
            vCV = self.pvecs_cov[region][checkpoint][:,:keep_features]
            vCK = self.pvecs_kur[region][checkpoint][:,:keep_features]
        # reconstruct the data
        rsCV = self.linear_reconstruction(data=scaled_data,
                                              vectors=vCV)
        rsCK = self.linear_reconstruction(data=scaled_data,
                                              vectors=vCK)
        # Data Rescaling
        rCV=self.mean_by_max_abs_unscaling(reconstructed_data=rsCV,
                                      original_data= data_subdomain)
        rCK=self.mean_by_max_abs_unscaling(reconstructed_data=rsCK,
                                      original_data= data_subdomain)

        # Remove the negative values from the data
        rCV = self.remove_negative_mass_fractions(rCV,
                non_participating_species)
        rCK = self.remove_negative_mass_fractions(rCK,
                non_participating_species)

        return data_subdomain, scaled_data, rCV, rCK, rsCV, rsCK


    def _get_reconstructed_data(self, scaled_data, original_data,
            vCV, vCK, retain_features=-1):
        '''
        '''
        keep_features = self.n_features if retain_features == -1 \
                    else retain_features
        vCV_R = vCV[:,:keep_features]
        vCK_R = vCK[:,:keep_features]
        # reconstruct the data
        rsCV = self.linear_reconstruction(data=scaled_data,
                                              vectors=vCV_R)
        rsCK = self.linear_reconstruction(data=scaled_data,
                                              vectors=vCK_R)
        # Data Rescaling
        rCV=self.mean_by_max_abs_unscaling(reconstructed_data=rsCV,
                                      original_data=original_data)
        rCK=self.mean_by_max_abs_unscaling(reconstructed_data=rsCK,
                                      original_data=original_data)

        return rCV, rCK, rsCV, rsCK

    def get_errors(self, region, xStart=None, xEnd=None,
            yStart=None, yEnd=None, zStart=None, zEnd=None,
            retain_features=-1, non_participating_species='N2'):
        '''
        '''
        # obtain data subdomain extents
        xs, ys, zs, xe, ye, ze = \
            self.obtain_subdomain_extents(xStart, yStart, zStart,
                                            xEnd, yEnd, zEnd)
        # create temporal error arrays
        err_array_shape = list([self.n_checkpoints])
        err_array_shape+= list([xe-xs, ye-ys, ze-zs])
        err_array_shape+= list([self.n_features])
        errCV = numpy.zeros(err_array_shape)
        errCK = numpy.zeros(err_array_shape)
        errCVscaled = numpy.zeros(err_array_shape)
        errCKscaled = numpy.zeros(err_array_shape)
        # evaluate the temporal evolution of the errors
        for idx, chkpt in enumerate(self.all_checkpoints):
            eCVs, eCKs, eCV, eCK = self.get_errors_at_checkpoint(
                checkpoint=chkpt, region=region,
                xStart=xs, yStart=ys, zStart=zs,
                xEnd=xe, yEnd=ye, zEnd=ze,
                retain_features=retain_features,
                non_participating_species=non_participating_species)
            errCVscaled[idx] = eCVs
            errCKscaled[idx] = eCKs
            errCV[idx] = eCV
            errCK[idx] = eCK
        return (errCVscaled.squeeze(), errCKscaled.squeeze(),
                errCV.squeeze(), errCK.squeeze())

    def get_errors_at_checkpoint(self, region,
            xStart=None, xEnd=None, yStart=None, yEnd=None,
            zStart=None, zEnd=None, retain_features=-1,
            non_participating_species='N2',
            checkpoint=None, external_file=None):

        oData, sData, rCV, rCK, rsCV, rsCK = \
            self.get_reconstructed_data_at_checkpoint(
                    region=region,
                    checkpoint=checkpoint,
                    external_file=external_file,
                    xStart=xStart, yStart=yStart, zStart=zStart,
                    xEnd=xEnd, yEnd=yEnd, zEnd=zEnd,
                    retain_features= retain_features,
                    non_participating_species=non_participating_species)

        errCVscaled = abs(sData - rsCV)
        errCKscaled = abs(sData - rsCK)

        max_abs, mean=self.evaluate_abs_max_and_mean(oData)

        errCV = abs(oData - rCV)
        errCK = abs(oData - rCK)

        #for feature in range(self.n_features):
        #    errCV[:,:,:, feature] /= max_abs[feature]
        #    errCK[:,:,:, feature] /= max_abs[feature]

        return errCVscaled, errCKscaled, errCV, errCK

    def remove_negative_mass_fractions(self, data,
            non_participating_species='N2'):
        '''
        nps = non participating species (default is nitrogen)
        '''
        nps = non_participating_species
        nps_index = self.valid_features.index(nps)

        for x in range(self.nx):
            for y in range(self.ny):
                for z in range(self.nz):
                    for v in range(self.n_features-1):
                        if data[x, y, z, v] < 0:
                            data[x, y, z, v] = 1e-16
                    total_mass = numpy.sum(data[x,y,z,:-1])
                    excess_mass = 1 - total_mass
                    data[x,y,z,nps_index] += excess_mass

        return data

    def set_training_pvals_pvecs(self, region, checkpoint):
        '''
        Method to set the training principal values and vectors
        to be used for the analysis of subsequent checkpoints
        '''
        self.global_pvals_cov = self.pvals_cov[region][checkpoint]
        self.global_pvecs_cov = self.pvecs_cov[region][checkpoint]
        self.global_pvals_kur = self.pvals_kur[region][checkpoint]
        self.global_pvecs_kur = self.pvecs_kur[region][checkpoint]

    def write_rates_from_s3D_output(self, method, s3d_datapath,
                                    outpath, checkpoint=None,
                                    external_file=None ):
        datafolder = '{}/s3d_{}'.format(s3d_datapath.rstrip("/"),
                                        method)
        rates=numpy.zeros((self.nx, self.ny, self.nz, self.n_features))
        for mpi in os.listdir(datafolder):
            if mpi.endswith('{:.4E}.mpi'.format(checkpoint)):
                data = numpy.fromfile('{}/{}'.format(datafolder,mpi))
                data = data.reshape(self.nz,self.ny,self.nx).T
                if mpi.startswith('hrr'):
                    rates[...,-1] = data
                else:
                    species_idx = int(mpi.split('.')[0].split('_')[1])
                    rates[...,species_idx] = data

        rates = rates.squeeze()
        outname = '{}/{}_rates.{:.4E}.npy'.format(outpath,method,checkpoint)
        numpy.save(outname,rates)
