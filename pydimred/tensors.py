'''Module to obtain the co-variance and co-kurtosis tensors 
'''
import numpy, numba

@numba.experimental.jitclass
class GetTensors:
    def __init__(self):
        pass

    def evaluate_tensors(self, data):
        nx, ny, nz, nv = data.shape
        # covariance tensor
        cov = numpy.zeros((nv, nv))
        # compute upper triangular matrix
        for i in range(nv):
            for j in range(i, nv):
                for x in range(nx):
                    for y in range(ny):
                        for z in range(nz):
                            cov[i, j] += data[x,y,z,i]*data[x,y,z,j]
        cov /= (nx*ny*nz)
        # obtain symmetric matrix
        cov_diag = numpy.diag(numpy.diag(cov))
        cov += cov.T
        cov -= cov_diag

        # note that since it is assumed that the data is centered
        # around the mean, it is not necessary to remove the
        # contribution of the first moment

        # raw kurtosis tensor
        cok = numpy.zeros((nv, nv, nv, nv))
        for i in range(nv):
            for j in range(nv):
                for k in range(nv):
                    for l in range(nv):
                        for x in range(nx):
                            for y in range(ny):
                                for z in range(nz):
                                    s = data[x,y,z,i]*data[x,y,z,j]*\
                                        data[x,y,z,k]*data[x,y,z,l]
                                    cok[i,j,k,l] += s
        cok /= (nx*ny*nz)
        # excess kurtosis
        for i in range(nv):
            for j in range(nv):
                for k in range(nv):
                    for l in range(nv):
                        cok[i,j,k,l] -= cov[i,j]*cov[k,l] 
                        cok[i,j,k,l] -= cov[i,k]*cov[j,l]
                        cok[i,j,k,l] -= cov[i,l]*cov[j,k]
        # since the first moment i.e. mean is assumed as zero even
        # the third moment is zero
        return cov, cok.reshape((nv, nv**3))
