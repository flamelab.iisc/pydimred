import numpy

class DataReconstruction:

    def linear_reconstruction(self, data, vectors):
        # obtain the lower rank data from scaled data
        recon_scaled_lower_rank = numpy.dot(data, vectors)
        # reconstruct full rank data from lower rank data
        recon_scaled = numpy.dot(recon_scaled_lower_rank, vectors.T)
        return recon_scaled


