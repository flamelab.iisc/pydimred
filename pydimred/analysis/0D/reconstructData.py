import sys
import numpy as np
import cantera as ct
import matplotlib.pylab as plt
import pandas as pd
import matplotlib

# load principal values and vectors computed at T=1200K and phi=0.4
S2 = np.load('pval2.npy')
S42 = np.load('pval42.npy')
U2 = np.load('pvec2.npy')
U42 = np.load('pvec42.npy')
print("Loaded Pvecs and Pvals computed at T=1200, phi=0.4")

# function to get truncated matrix
def truncateVecs(nv,q,U):
    U_q = np.zeros((nv,q), dtype=float, order='F')
    U_q = np.copy(U[:,0:q,])
    return U_q

# function to construct PC data
def constructPCData(nx,q,Xs,Uq):
    Z_q = np.zeros((nx,q), dtype=float, order='F')
    Z_q = np.matmul(Xs,Uq)
    return Z_q

# function to reconstruct original data
def reconstructData(nv,nx,Zq,Uq):
    X_sq = np.zeros((nx,nv), dtype=float, order='F')
    X_sq = np.matmul(Zq,Uq.transpose())
    return X_sq

# unscaling function
def unscaleData(nf, vs, v, temperature, phi):
    [v_mean, v_max] = np.load(
                      'T{}/{}/scaling.npy'.format(temperature, phi))
    # add for loop to scale the data: NORMALIZATION
    for i in range(nf):
        v[:, i] = (vs[:, i] * v_max[i]) + v_mean[i]
    return None

def replace_negative_mass_fractions(data, n_variables):
    for variable in range(n_variables):
        x_negative = np.where(data[...,variable] < 0)[0]
        #print(variable, len(x_negative))
        if len(x_negative) == 0: continue
        else:
            data[x_negative, variable] = 1e-16
    return data

# scaling function
def scaleData(data, temperature, phi):
    nx, nv = data.shape
    # compute mean
    v_mean = np.mean(data, axis=0)
    # compute max
    v_max = np.max(np.abs(data), axis=0)

    np.save('T{}/{}/scaling.npy'.format(temperature, phi),
            [v_mean, v_max])

    # add for loop to scale the data: NORMALIZATION
    for i in range(nv):
        data[:, i] = (data[:, i] - v_mean[i])/v_max[i]
    np.save('T{}/{}/Xs.npy'.format(temperature, phi), data)

def thermochemical_scalars(q, temperature, phi):
    # pressure
    p = np.load('T{}/{}/p.npy'.format(temperature, phi))
    # load mass fractions and temperature
    X = np.load('T{}/{}/Y_T.npy'.format(temperature, phi))
    nx, nv = np.shape(X)
    # scale the data
    Xs = np.zeros((nx,nv))
    Xs = np.copy(X)
    scaleData(Xs, temperature, phi)
    # reduce the covariance manifold
    U2q = np.zeros((nv,q), dtype=float, order='F')
    Z2q = np.zeros((nx,q), dtype=float, order='F')
    Xs2q = np.zeros((nx,nv), dtype=float, order='F')

    U2q = truncateVecs(nv,q,U2)
    Z2q = constructPCData(nx,q,Xs,U2q)
    Xs2q = reconstructData(nv,nx,Z2q,U2q)
    # compute absolute error in the reduced covariance manifold
    Es2 = np.absolute(Xs-Xs2q)

    # reduce the covariance manifold
    U42q = np.zeros((nv,q), dtype=float, order='F')
    Z42q = np.zeros((nx,q), dtype=float, order='F')
    Xs42q = np.zeros((nx,nv), dtype=float, order='F')

    U42q = truncateVecs(nv,q,U42)
    Z42q = constructPCData(nx,q,Xs,U42q)
    Xs42q = reconstructData(nv,nx,Z42q,U42q)

    # compute absolute error in the reduced cokurtosis manifold
    Es42 = np.absolute(Xs-Xs42q)

    # evaluate max error in the reduced manifolds
    Max2 = np.max(Es2,axis=0)
    Max42 = np.max(Es42,axis=0)

    # evaluate max error in the reduced manifolds
    Mean2 = np.mean(Es2, axis=0)
    Mean42 = np.mean(Es42, axis=0)

    # covariance data
    X2q = np.zeros((nx,nv)) # scaled data matrix
    unscaleData(nv,Xs2q,X2q, temperature, phi)

    # cokurtosis data
    X42q = np.zeros((nx,nv)) # scaled data matrix
    unscaleData(nv,Xs42q,X42q, temperature, phi)

    X2q  = replace_negative_mass_fractions(data=X2q, n_variables=nv)
    X42q = replace_negative_mass_fractions(data=X42q, n_variables=nv)

    # make mass fractions 1
    excess_mass_2q = 1 - np.sum(X2q[:,:-1], axis=1)
    excess_mass_42q= 1 - np.sum(X42q[:,:-1],axis=1)

    X2q[ :, -2] += excess_mass_2q
    X42q[:, -2] += excess_mass_42q

    # check mass fractions
    assert(np.all(np.isclose(np.sum(X2q[:,:-1], axis=1), np.ones(nx))))
    assert(np.all(np.isclose(np.sum(X42q[:,:-1],axis=1), np.ones(nx))))

    np.save('T{}/{}/X2q{}'.format(temperature, phi, q), X2q )
    np.save('T{}/{}/X42q{}'.format(temperature, phi, q),X42q)

    # compute errors
    eX2q = np.abs(X-X2q)
    eX42q = np.abs(X-X42q)

    eXMax2q = np.max(eX2q,axis=0)
    eXMax42q = np.max(eX42q,axis=0)

    eXMean2q = np.mean(eX2q,axis=0)
    eXMean42q = np.mean(eX42q,axis=0)

    np.save('T{}/{}/eXMax2q{}'.format(temperature,phi,q), eXMax2q)
    np.save('T{}/{}/eXMax42q{}'.format(temperature,phi,q), eXMax42q)
    np.save('T{}/{}/eXMean2q{}'.format(temperature,phi,q), eXMean2q)
    np.save('T{}/{}/eXMean42q{}'.format(temperature,phi,q),eXMean42q)

    return X2q, X42q, p

def production_heatrealese_rates(X2q, X42q, p, q, temperature, phi):
    gas = ct.Solution('chem.cti')

    # load heat release rate, production and reaction rates
    HRR = np.load('T{}/{}/HRR.npy'.format(temperature, phi))
    PR = np.load('T{}/{}/PR.npy'.format(temperature, phi))
    RR = np.load('T{}/{}/RR.npy'.format(temperature, phi))

    nstep = 2500;
    nt = nstep+1
    nx = nt
    nsc = gas.Y.size
    nv = nsc + 1

    nRR = gas.n_reactions        # number of reactions
    RR2q = np.zeros((nt,nRR))   # reaction rates matrix
    RR42q = np.zeros((nt,nRR))   # reaction rates matrix

    PR2q = np.zeros((nt,nsc))   # production rates matrix
    PR42q = np.zeros((nt,nsc))   # production rates matrix

    HRR2q = np.zeros(nt)
    HRR42q = np.zeros(nt)

    # compute reaction rates at each time step
    for it in range(nt):
        gas.TP = X2q[it,nv-1],p[it]
        gas.Y = X2q[it,:nsc]
        for ir in range(nRR):
            RR2q[it,ir] = gas.net_rates_of_progress[ir]

        for iv in range(nsc):
            PR2q[it,iv] = gas.net_production_rates[iv]

        HRR2q[it] =- np.sum(gas.net_production_rates[:] * gas.partial_molar_enthalpies[:], axis=0)

    # compute reaction rates at each time step
    for it in range(nt):
        gas.TP = X42q[it,nv-1],p[it]
        gas.Y = X42q[it,:nsc]

        for iv in range(nsc):
            PR42q[it,iv] = gas.net_production_rates[iv]

        HRR42q[it] =- np.sum(gas.net_production_rates[:] * gas.partial_molar_enthalpies[:], axis=0)

    np.save('T{}/{}/HRR2q{}'.format(temperature, phi, q), HRR2q)
    np.save('T{}/{}/HRR42q{}'.format(temperature, phi,q),HRR42q)
    np.save('T{}/{}/PR2q{}'.format(temperature, phi,q), PR2q)
    np.save('T{}/{}/PR42q{}'.format(temperature, phi,q),PR42q)

    ePR2q = np.abs(PR-PR2q)
    ePR42q = np.abs(PR-PR42q)
 
    eHRR2q = np.abs(HRR-HRR2q)
    eHRR42q = np.abs(HRR-HRR42q)

    ePRMax2q = np.max(ePR2q,axis=0)
    ePRMax42q = np.max(ePR42q,axis=0)

    ePRMean2q = np.mean(ePR2q, axis=0)
    ePRMean42q = np.mean(ePR42q, axis=0)

    eHRRMax2q = np.max(eHRR2q,axis=0)
    eHRRMax42q = np.max(eHRR42q,axis=0)

    eHRRMean2q = np.mean(eHRR2q, axis=0)
    eHRRMean42q = np.mean(eHRR42q, axis=0)

    np.save('T{}/{}/ePRMax2q{}'.format(temperature, phi,q), ePRMax2q)
    np.save('T{}/{}/ePRMax42q{}'.format(temperature, phi,q), ePRMax42q)
    np.save('T{}/{}/ePRMean2q{}'.format(temperature, phi,q), ePRMean2q)
    np.save('T{}/{}/ePRMean42q{}'.format(temperature, phi,q),ePRMean42q)
    np.save('T{}/{}/eHRRMax2q{}'.format(temperature, phi,q), eHRRMax2q)
    np.save('T{}/{}/eHRRMax42q{}'.format(temperature, phi,q), eHRRMax42q)
    np.save('T{}/{}/eHRRMean2q{}'.format(temperature, phi,q), eHRRMean2q)
    np.save('T{}/{}/eHRRMean42q{}'.format(temperature, phi,q),eHRRMean42q)

def write_data(q, temperature, phi):
    X2q, X42q, p = thermochemical_scalars(q, temperature, phi)
    production_heatrealese_rates(X2q, X42q, p, q, temperature, phi)
    print("Written data for q={}, T={} & phi={}".format(q, temperature, phi))

def run():
    for q in [5,15]:
        for temp in range(1150, 1251, 50):
            for phi in [.375, 0.4, .425]:
                write_data(q, str(temp), str(phi))

if __name__ == "__main__":
    run()
