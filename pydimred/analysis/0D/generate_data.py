import numpy as np
import cantera as ct
import matplotlib.pyplot as plot
import os

# composition of oxidizer (air)
air = "O2:21, N2:79"

def compute_slope(Xi, dt):
    slope = np.zeros_like(Xi)
    slope[0] = -3*Xi[0] + 4*Xi[1] - Xi[2]
    slope[-1]= 3*Xi[-1] - 4*Xi[-2]+ Xi[-3]
    for i in range(1, len(Xi)-1):
        slope[i] = Xi[i+1] - Xi[i-1]
    slope /= (2*dt)
    return slope

def generate_data(temperature=1200.0, phi=0.4, dtime=1.e-6,
                  nstep=2500):
    # create solution object
    gas = ct.Solution('chem.cti')
    # create output directory for specified temperature and
    # equivalence ratio
    try: os.mkdir("T{}".format(int(temperature)))
    except FileExistsError: pass
    try: os.mkdir("T{}/{}".format(int(temperature), phi))
    except FileExistsError: pass
    if all([temperature == 1200.0, phi == 0.4]):
        gas.TPY = 1200.0, ct.one_atm*1.72, 'C2H4:0.02652, N2:0.74656, O2:0.22692'
    else:
        # set temperature and pressure
        gas.TP = temperature, ct.one_atm*1.72
        # set equivalence ratio in the solution object
        gas.set_equivalence_ratio(phi, fuel="C2H4", oxidizer=air)
    # create ODE reactor
    r = ct.IdealGasConstPressureReactor(gas)
    # create data matrices (size NTxNV)
    nt = nstep+1
    nx = nt
    nsc = gas.Y.size
    nv = nsc + 1

    X = np.zeros((nt,nv))                  # data matrix
    p = np.zeros((nt))                     # pressure matrix
    nRR = gas.n_reactions                  # number of reactions
    RR = np.zeros((nt,nRR), dtype=float)   # reaction rates matrix
    PR = np.zeros((nt,nsc), dtype=float)   # production rates matrix
    HRR = np.zeros((nt))                   # Heat Release rates matrix

    # simulation object
    sim = ct.ReactorNet([r])
    time = np.zeros(nt)

    # initial condition
    for i in range(nsc):
        X[0,i] = gas.Y[i]
    X[0,nv-1] = gas.T
    p[0] = gas.P

    for n in range(nstep):
        time[n+1]=time[n]+dtime
        # integrates the solution in time
        sim.advance(time[n+1])
        # append data to X matrix
        for i in range(nsc):
            X[n+1,i] = gas.Y[i]
        X[n+1,nv-1] = gas.T
        p[n+1] = gas.P
    # save massfractions and temperature
    try:
        np.save('T{}/{}/Y_T.npy'.format(int(temperature), phi),X)
        np.save('T{}/{}/p.npy'.format(int(temperature), phi),p)
    except FileExistsError: pass
        
    # compute reaction rates at each time step
    for it in range(nt):
        gas.TP = X[it,nv-1],p[it]
        gas.Y = X[it,:nsc]
        for ir in range(nRR):
            RR[it,ir] = gas.net_rates_of_progress[ir]
                
        for iv in range(nsc):
            PR[it,iv] = gas.net_production_rates[iv]

        HRR[it] =- np.sum(gas.net_production_rates[:] * gas.partial_molar_enthalpies[:], axis=0)

    # save massfractions and temperature
    try:
        np.save('T{}/{}/RR.npy'.format(int(temperature), phi),RR)
        np.save('T{}/{}/PR.npy'.format(int(temperature), phi),PR)
        np.save('T{}/{}/HRR.npy'.format(int(temperature), phi),HRR)
    except FileExistsError: pass

if __name__ == "__main__":
    dt = dict()
    dt[1150] = dict({0.375:2e-6, 0.4:   2e-6, 0.425:1.75e-6})
    dt[1200] = dict({0.375:1e-6, 0.4:   1e-6, 0.425:   1e-6})
    dt[1250] = dict({0.375:6e-7, 0.4:5.75e-6, 0.425:5.75e-6})

    with open('parameters', 'w') as f:
        for T in [1150, 1200, 1250]:
            for phi in [0.375, 0.4, 0.425]:
                dtime = dt[T][phi]
                params = "{},{},{}\n".format(T, phi, dtime)
                f.write(params)
                generate_data(temperature=T, phi=phi, dtime=dtime)
