from glob import glob

def setup_package():
    from setuptools import setup, find_packages

    setup(
        name='pydimred',
        description='A Python based dimensionality reduction of combustion datasets',
        author='Anirudh Jonnalagadda',
        author_email='jonnalagadda.anirudh@gmail.com',
        packages=find_packages(),
        data_files = [("syngas",
                        glob("pydimred/datasets/syngas/*.mpi")),
                      ("hcci",
                        glob("pydimred/datasets/hcci/*.mpi")),
                      ("hcci_ref",
                        glob("pydimred/datasets/hcci/hcci_ref.npy"))],
        include_package_data=True
        )

if __name__ == '__main__':
    setup_package()
