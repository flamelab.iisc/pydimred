# pydimred

Dimensionality reduction based on Co-kurtosis for multivariate datasets (Cok-PCA):

# Developers:

Anirudh Jonnalagadda
Konduri Aditya

# Description:
A software suite to perform dimensionality reduction based on high-order joint moment, co-kurtosis, for multivariate datasets.
The method has been shown work well for datasets where anomalous or extreme-valued events are important and need to be captured.
The software also comprises an evaluation method based on linear reconstruction and an example execution pipeline.
